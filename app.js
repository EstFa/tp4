
//3 - Afficher les ingrédients des pizzas au survol de leurs noms


$(function(){

	$('.type.pizza-type label').hover(
	function () {
		$(this).find('span').removeClass('no-display'); 
	}, function () {
		$(this).find('span').addClass('no-display');
	});



	//4 - Afficher visuellement le nombre de parts et le nombre de pizzas approprié en dessous du champs « Nombre de parts »


	$('.type.nb-parts input').change(function(){

		var valeur = $('.type.nb-parts input').val();
    
    	$("span.pizza-deux").remove();


    	if(valeur > 6){
    		$("span.pizza-une").removeClass().addClass("pizza-une").addClass("pizza-pict").addClass("pizza-6");
    		$('.type.nb-parts').append('<span class="pizza-'+(valeur - 6)+' pizza-pict pizza-deux"></span>');
   	 	} else {
    		 $("span.pizza-une").removeClass().addClass("pizza-une").addClass("pizza-pict").addClass("pizza-"+valeur);
    		}
   
	})


	//5 - Afficher le formulaire de saisie d'adresse au clic sur le bouton "Etape suivante" puis masquer ce même bouton


	
	$('button.next-step').click(function(){
		$("button.next-step").css("display","none");
		$(".infos-client").removeClass("no-display");
	})

	//6 - Ajouter une ligne de champ d'adresse lorsque l'on clique sur le bouton "Ajouter un autre ligne d'adresse"

	$('button.btn-default').click(function(){

		$(this).parent().prepend('<input type="text"/>')
	
	})

	//7 -Au clic sur le bouton de validation, supprimer tous les éléments de la page, et afficher un message de remerciement (Merci PRENOM ! Votre commande sera livrée dans 15 minutes).

	$('button.done').click(function(){


		var prenom = $(".infos-client input").first().val();
		$('button.done').css("display","none");
		$('.main').css("display","none");
		$('.headline').append("<p> Merci "+prenom+" ! Votre commande sera livrée dans 15 minutes </p>");
	
	})



	//8 - Actualiser le total de la commande en fonction des éléments choisis grâce à l'attribut data-price. Mettre ce calcul dans une fonction (DRY - Don’t Repeat Yourself)


    
	var pizza=0
	var pate =0
	var extra=0
	

	$(".type.pizza-type label").click(function(){

		pizza=$("input[name='type']:checked").data("price");
		console.log("prix pizza="+pizza);
		afficher();
	})


	$("input[name='pate']").click(function(){

		pate=$("input[name='pate']:checked").data("price");
		console.log("pate="+pate);
		afficher();
	})

	$("input[name='extra']").click(function(){

		var extra1,extra2,extra3,extra4;
		if($("input[name='extra'][value='1']").is(':checked')){
			extra1=$("input[name='extra'][value='1']").data("price");
		}else{
			extra1=0;
		}
		if($("input[name='extra'][value='2']").is(':checked')){
			extra2=$("input[name='extra'][value='2']").data("price");
		}else{
			extra2=0;
		}
		if($("input[name='extra'][value='3']").is(':checked')){
			extra3=$("input[name='extra'][value='3']").data("price");
		}else{
			extra3=0;
		}
		if($("input[name='extra'][value='4']").is(':checked')){
			extra4=$("input[name='extra'][value='4']").data("price");
		}else{
			extra4=0;
		}
		extra=extra1+extra2+extra3+extra4;
		console.log("extra="+extra);
		afficher();
	})

	function afficher(){
		var prix=pizza+pate+extra;
		$(".stick-right .tile p").replaceWith("<p>"+prix+"€</p>");
	}
})






